<?php

/**
 * @file
 * Enable a navigation block for each taxonomy category and enable the 
 * possibility to set a base link for each link passing the term id as parameter.
 */

/**
 * Hook_block.
 * Creates a navigator block for each of the vocabularies.
 */
function taxonomy_navigator_block($op = 'list', $delta = 0, $edit = array()) {
  $vocabularies = taxonomy_get_vocabularies();
  switch($op) {
    case 'list':
      $blocks = array();
      foreach($vocabularies as $vid => $vocabulary) {
        $blocks[$vid] = array(
          'info' => t('Taxonomy Navigation for @vocabulary', 
            array('@vocabulary' => $vocabulary->name)),
        );
      }
      return $blocks;
    case 'configure':
      $form['base_path'] = array(
        '#type' => 'textfield',
        '#title' => t('Base path for the links'),
        '#default_value' => variable_get(
            'taxonomy_navigator_base_path_' . $delta, 
            'taxonomy/term/'),
      );
      return $form;
    case 'save':
      variable_set('taxonomy_navigator_base_path_' . $delta, $edit['base_path']);
      break;
    case 'view':
      $block = array(
        'title' => $vocabularies[$delta]->name,
        'content' => taxonomy_navigator_block_content($delta),
      );
      return $block;
  }
}

/**
 * Creates the content with the links for a block for a specific vocabulary.
 *
 * @param int $vid The vocabulary id
 * @return string The content for the block.
 */
function taxonomy_navigator_block_content($vid) {
  $terms = taxonomy_get_tree($vid, 0, -1, 1);
  $base_path = variable_get('taxonomy_navigator_base_path_' . $vid, 'taxonomy/term/');
  $links = array();
  foreach($terms as $term) {
    $links[$term->tid] = array(
        'title' => $term->name, 
        'href' => $base_path . $term->tid,
    );
    if(taxonomy_navigator_is_term_active($vid, $term->tid)) {
      $links[$term->tid]['attributes'] = array('class' => 'active');
    }
  }
  return theme('links', $links, array('class' => 'menu'));
}

/**
 * Check whenever or not the term id is active, whenever is the base path plus
 * the term id or is a node containing this term id.
 *
 * @param int $vid The vocabulary id for the term id.
 * @param int $tid The term id.
 * @return boolean Whenever or not this term is active.
 */
function taxonomy_navigator_is_term_active($vid, $tid) {
  $actual_path = $_GET['q'];
  $base_path = variable_get('taxonomy_navigator_base_path_' . $vid, 'taxonomy/term/');
  if($actual_path == $base_path . $tid) {
    return true;
  }
  if(arg(0) == 'node' && is_numeric(arg(1))){
    $nid = arg(1);
    $terms = taxonomy_node_get_terms_by_vocabulary($nid, $vid);
    if(key_exists($tid, $terms)) {
      return true;
    }
  }
  return false;
}